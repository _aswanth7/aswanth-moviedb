const bodyParser = require("body-parser");
const express = require("express");
const morgan = require("morgan");
const fs = require("fs");
const path = require("path");
const http = require("http");

const db = require("./app/model/index");
const router = require("./app/routes/routes");
const migartion = require("./app/model/migrateData");

const app = express();
const PORT = process.env.PORT || 4000;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//logging the request into console and file
var fileLogging = fs.createWriteStream(path.join(__dirname, "app.log"), {
  flags: "a",
});
app.use(morgan("dev", { stream: fileLogging }));

app.use("/api/movies", router);

//error handling for undefined apis
app.use((req, res, next) => {
  let error = new Error("Not Found");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  errorCode = error.status || 500;
  res.status(errorCode).send(http.STATUS_CODES[errorCode]).end();
});

db.sequelize.sync().then(() => {
  console.log("Database is created");
  // migartion();
});
app.listen(PORT, console.log(`Server started on port ${PORT}`));
