const { movies } = require("../model");
const db = require("../model");

const Movies = db.movies;
const op = db.Sequelize.Op;

create = (req, res) => {
  console.log(req.body);
  if (
    !req.body.rank ||
    !req.body.title ||
    !req.body.runtime ||
    !req.body.genre ||
    !req.body.director ||
    !req.body.actor ||
    !req.body.year
  ) {
    res
      .status(401)
      .send({
        message: " Required rank, title, runtime, genre, director, actor,year",
      })
      .end();
  } else {
    const movies = {
      title: req.body.title,
      rank: req.body.rank,
      description: req.body.description,
      runtime: req.body.runtime,
      genre: req.body.genre,
      rating: req.body.rating,
      metascore: req.body.metascore,
      votes: req.body.votes,
      gross: req.body.gross,
      director: req.body.director,
      actor: req.body.actor,
      year: req.body.year,
    };
    Movies.create(movies)
      .then((data) => res.send(data))
      .catch((err) => {
        message =
          err.errors != undefined
            ? `${err.errors[0].type} ${err.errors[0].message} `
            : err.parent.sqlMessage;
        res
          .status(500)
          .send({
            message,
          })
          .end();
      });
  }
};
findAll = (req, res) => {
  const condition = req.query.title
    ? { title: { [op.like]: `%${req.query.title}%` } }
    : null;

  movies
    .findAll({ where: condition })
    .then((data) => {
      res.status(200).send(JSON.stringify(data)).end();
    })
    .catch((err) => {
      res
        .status(500)
        .send({
          message: err.message,
        })
        .end();
    });
};

findOne = (req, res) => {
  const id = req.params.id;
  movies
    .findByPk(id)
    .then((data) => {
      res.status(200).send(JSON.stringify(data)).end();
    })
    .catch((err) => {
      res
        .status(500)
        .send({
          message: err.message,
        })
        .end();
    });
};
destroy = (req, res) => {
  const id = req.params.id;
  movies
    .destroy({
      where: { id: id },
    })
    .then((isDeleted) => {
      if (isDeleted == 1)
        res.status(200).send(`Movie with id ${id} is deleted`).end();
      else
        res.status(200).send(` Problem in deleting Movie with id ${id}`).end();
    })
    .catch((err) => {
      res
        .status(500)
        .send({
          message: err.message,
        })
        .end();
    });
};
update = (req, res) => {
  const id = req.params.id;

  movies
    .update(req.body, {
      where: { id: id },
    })
    .then((isUpdated) => {
      if (isUpdated == 1)
        res.status(200).send(`Movie with id ${id} is Updated`).end();
      else
        res.status(200).send(` Problem in Updating Movie with id ${id}`).end();
    })
    .catch((err) => {
      res
        .status(500)
        .send({
          message: err.message,
        })
        .end();
    });
};

module.exports = { create, findAll, findOne, update, destroy };
