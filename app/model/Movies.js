module.exports = (sequelize, Sequelize) => {
  const Movies = sequelize.define("movies", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    title: {
      type: Sequelize.STRING,
    },
    rank: {
      type: Sequelize.INTEGER,
      unique: true,
    },
    description: {
      type: Sequelize.STRING,
      defaultValue: "null",
    },
    runtime: {
      type: Sequelize.INTEGER,
    },
    genre: {
      type: Sequelize.STRING,
    },
    rating: {
      type: Sequelize.FLOAT(2, 1),
      defaultValue: null,
    },
    metascore: {
      type: Sequelize.INTEGER,
      defaultValue: null,
    },
    votes: {
      type: Sequelize.INTEGER,
      defaultValue: null,
    },
    gross: {
      type: Sequelize.FLOAT(7, 4),
      defaultValue: null,
    },
    director: {
      type: Sequelize.STRING,
    },
    actor: {
      type: Sequelize.STRING,
    },
    year: {
      type: Sequelize.INTEGER,
    },
  });
  return Movies;
};
