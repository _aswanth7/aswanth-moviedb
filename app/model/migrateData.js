const fs = require("fs");
const path = require("path");
const db = require("../model");
const Movies = db.movies;
module.exports = migrateData = () => {
  fs.readFile(
    path.join(__dirname, "../", "data", "movies.json"),
    function (err, data) {
      if (err) console.log(err);
      const jsonParsed = JSON.parse(data);
      jsonParsed.forEach((element) => {
        const movies = {
          title: element.Title,
          rank: element.Rank,
          description: element.Description,
          runtime: element.Runtime,
          genre: element.Genre,
          rating: element.Rating,
          metascore: element.Metascore == "NA" ? null : element.Metascore,
          votes: element.Votes,
          gross: element.Gross_Earning_in_Mil,
          director: element.Director,
          actor: element.Actor,
          year: element.Year,
        };
        Movies.create(movies)
          .then()
          .catch((err) => {
            console.error("Data  population failed", err);
          });
      });
    }
  );
};
