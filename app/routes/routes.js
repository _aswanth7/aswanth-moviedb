const express = require("express");
const router = express.Router();

const movies = require("../controller/movies");

router.post("/", movies.create);
router.get("/", movies.findAll);

router.get("/:id", movies.findOne);
router.delete("/:id", movies.destroy);
router.put("/:id", movies.update);

module.exports = router;
